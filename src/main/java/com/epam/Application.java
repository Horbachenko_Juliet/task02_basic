package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Create number set of entered interval.
 * Finding summ of even and odd numbers.
 */
public class Application {

    public static void main(String[] args) {

        int startInterval;
        int endInterval;
        int sumOdd = 0;
        int sumEven = 0;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the first number of interval");
            startInterval = Integer.parseInt(reader.readLine());
            System.out.println("Enter the last number of interval");
            endInterval = Integer.parseInt(reader.readLine());

            if (startInterval > endInterval) {
                int tmp = startInterval;
                startInterval = endInterval;
                endInterval = tmp;
            }

            System.out.print("Entered interval is: ");
            System.out.println("[" + startInterval + ";" + endInterval + "]");
            System.out.print("Even numbers:");

            for (int i = startInterval; i <= endInterval; i++) {
                if (i % 2 == 0 && i != 0) {
                    System.out.print(i + " ");
                    sumEven += i;
                }
            }
            System.out.println();

            System.out.println("Sum of even numbers: " + sumEven);

            System.out.print("Odd numbers:");

            for (int i = startInterval; i <= endInterval; i++) {
                if (i % 2 != 0) {
                    System.out.print(i + " ");
                    sumOdd += i;
                }
            }
            System.out.println();

            System.out.println("Sum of odd numbers: " + sumOdd);

        } catch (NumberFormatException e) {
            System.out.println("Enter a number, please");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Fibonacci fibonacciSet = new Fibonacci();
        fibonacciSet.printSet();
        fibonacciSet.printBiggestOddNumber();
        fibonacciSet.printBiggestEvenNumber();
    }
}
