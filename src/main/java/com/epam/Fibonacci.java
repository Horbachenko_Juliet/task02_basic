package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * Create Fibonacci numbers set.
 * Find biggest odd and even numbers and their percentage.
 *
 */
public class Fibonacci {

    private static final int HUNDRED = 100;
    private long n1 = 0;
    private long n2 = 1;
    private long n3;
    private int sizeOfSet;
    private LinkedList<Long> numberSet = new LinkedList<Long>();

    /**
     * Create numberSet.
     */
    void printSet() {
        try {
            System.out.println("Please, enter size the Fibonacci size set:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            sizeOfSet = Integer.parseInt(reader.readLine());
            numberSet.add(n1);
            numberSet.add(n2);
            for (int i = 2; i < sizeOfSet; i++) {
                n3 = n1 + n2;
                numberSet.add(n3);
                n1 = n2;
                n2 = n3;
            }
            System.out.println("Fibonacci numbers :" + numberSet);
        } catch (NumberFormatException e) {
            System.out.println("Invalid number");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private float calculatePercentage(int amount, int total) {
        return (float) amount * HUNDRED / total;
    }

    void printBiggestEvenNumber() {
        long max = 0;
        int amount = 0;
        for (Long n : numberSet) {
            if (n % 2 == 0 && n > max) {
                max = n;
                amount += 1;
            }
        }
        System.out.println("The biggest even number is: " + max);
        System.out.print("Even percentage: ");
        System.out.println(calculatePercentage(amount, numberSet.size()));
    }

    void printBiggestOddNumber() {
        long max = 0;
        int amount = 0;
        for (Long n : numberSet) {
            if (n % 2 != 0 && n > max) {
                max = n;
                amount += 1;
            }
        }

        System.out.println("The biggest odd number is: " + max);
        System.out.print("Odd percentage: ");
        System.out.println(calculatePercentage(amount, numberSet.size()));
    }
}
